# Задача: Организация CI/CD на GitLab

## Описание задачи

Необходимо организовать проект с системой контроля версий и системой сборки на основе CI/CD. Система CI/CD должна собирать приложение и запускать его, принимая аргумент, который может динамически указываться пользователем. Результат работы приложения должен быть возвращен из CI/CD в виде артефакта.

Также требуется создать приложение, которое генерирует пароль от 8 до 32 символов, включающий буквы, цифры и спецсимволы.

## Требования к репозиторию

1. Создать публичный репозиторий на GitLab.

2. Оформить структуру проекта на любом языке программирования и сделать первоначальную фиксацию пустого проекта.

3. Оформить ветки "master" и "dev".

4. Зафиксировать в ветке "dev" промежуточные этапы разработки.

5. В ветку "master" зафиксировать версию 1.0 - полностью готовое решение задачи.

6. Создать документацию:
    - В корне проекта: файл README.md с кратким описанием и интерфейсом программы в разметке markdown.
    - В корне проекта: файл DOC.md с описанием задачи в разметке markdown.

## Функциональные требования

1. Решить задачу на любом языке программирования.

2. Решения всех задач должны собираться, запускаться или разворачиваться системой CI/CD.

3. Требуется ввод данных, организовать данный ввод динамически средствами CI/CD, а не "жестко закодировать".