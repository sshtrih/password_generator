import random
import string
import os


password_length = int(os.getenv("PASSWORD_LENGTH", 16))

def generate_password(length=16):
    if not (8 <= length <= 32):
        raise ValueError("Password length must be between 8 and 32 characters")

    # Генерация пароля с использованием букв, цифр и специальных символов
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for _ in range(length))

    return password

if __name__ == "__main__":
    password = generate_password(password_length)
    with open("password.txt", "w") as f:
        f.write(password)

